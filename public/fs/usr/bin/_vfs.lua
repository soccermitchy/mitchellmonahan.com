local json = require'lua.json'
local js = require'js'
local window = js.global
return function()
    window.console:log("VFS Contents", window.JSON:parse(json.encode(Vfs.inodes)))
    return "JSON tree printed to browser console."
end