return function()
    return table.concat({
        "<div>", 
        "<p><b>Welcome to mitchellmonahan.com</b></p>", 
        "<p>This page is (loosely) based on <a href=\"https://github.com/louh/fake-terminal\">this</a> github project. (Hey, I never said I was a web designer)</p>",
        "<p>The largest difference between the code there and here is that I rewrote the javascript there into Lua, running on <a href=\"https://fengari.io\">Fengari</a></p>",
        "<p>The source code for this site is <a href=\"https://gitlab.com/soccermitchy/mitchellmonahan.com\">over here</a>. Not very interesting though. You could</p>", 
        "<p>just grab it from your browser's dev tools.</p>", 
        "<p>This is a quick list of <u>some</u> supported commands.</p>", 
        "<ul>", 
        "<li><strong>help</strong> - This.</li>", 
        "<li><strong>echo &lt;string&gt;</strong> - Echos some text</li>", 
        --"<li><strong>exit</strong> - Exit the terminal</li>", 
        "<li><strong>aboutme</strong> - Some information about me</li>", 
        "</ul></div>"
    })
end