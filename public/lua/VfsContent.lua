Vfs:init()
Vfs:createDirectory("/bin")
Vfs:createDirectory("/usr")
Vfs:createDirectory("/usr/bin")
--[[getDataFromUrl("fs/usr/bin/shell.lua", function(data)
    Vfs:createFile("/usr/bin/shell", data.response)
end)]]
local fsMappings = {
--  ["local path"] = "remote path"
    ["/bin/shell"] = "fs/bin/shell.lua",
    ["/bin/echo"] = "fs/bin/echo.lua",
    ["/bin/help"] = "fs/bin/help.lua",
    ["/usr/bin/aboutme"] = "fs/usr/bin/aboutme.lua",
    ["/usr/bin/_vfs"] = "fs/usr/bin/_vfs.lua",
}

for k,v in pairs(fsMappings) do
    getDataFromUrl(v, function(d)
        Vfs:createFile(k, d.response)
    end)
end