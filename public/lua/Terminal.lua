local js = require"js"
local window = js.global
local document = window.document
local console = window.console

Terminal = {
    history = {},
    historyIndex = 0,
    KEY_UP = 38,
    KEY_DOWN = 40,
    KEY_TAB = 9,
    opts = {
        commands = {},
        prompt = "[unset] $ ",
        intro = "",
        user = "nobody"
    }
}
function Terminal:resetPrompt(t, p)
    local np = p.parentNode:cloneNode(true)
    p:setAttribute('contenteditable', false)
    np:querySelector('.terminal-prompt').textContent = self:getPrompt(self.opts.prompt, self.opts)
    t:appendChild(np)
    np:querySelector('.terminal-input').innerHTML = ' '
    np:querySelector('.terminal-input'):focus()
end
function Terminal:tryCommand(term, cmd, args)
    local searchPath = {
        "/bin",
        "/usr/bin"
    }
    for k,v in ipairs(searchPath) do
        local err, data = Vfs:getEnt(v.."/"..cmd)
        if err == Vfs.E_OK then
            local func, err = load(data.content)
            if func then
                term.innerHTML = term.innerHTML .. (func()(args) or "")
            else
                term.innerHTML = term.innerHTML .. "<span class='terminal-error'>Error executing command - see browser console</span>"
                print(err)
            end
            return
        end
    end
    if (self.opts.commands[cmd]) then
        print("Using shell builtin for " .. cmd)
        self:runCommand(term, cmd, args)
    else
        self:commandNotFound(term, cmd)
    end
end
function Terminal:runCommand(term, cmd, args)
    term.innerHTML = term.innerHTML .. self.opts.commands[cmd](args)
end
function Terminal:commandNotFound(term, cmd)
    term.innerHTML = term.innerHTML .. cmd .. ": command not found"
end
function Terminal:updateHistory(cmd)
    table.insert(self.history, cmd)
    historyIndex = #self.history
end
function Terminal:browseHistory(prompt, dir)
    local changedPrompt = false
    if (direction == self.KEY_UP and self.historyIndex > 0) then
        self.historyIndex = self.historyIndex - 1
        prompt.textContent = self.history[self.historyIndex]
        changedPrompt = true
    else
        if (self.historyIndex < #self.history) then 
            self.historyIndex = self.historyIndex + 1
        else
            prompt.textContent = " "
        end
        changedPrompt = true
    end
    if (changedPrompt) then
        local range = document:createRange()
        local sel = window:getSelection()
        range:setStart(prompt.childNodes[0], #prompt.textContent)
        range:collapse(true)
        sel:removeAllRanges()
        sel:addRange(range)
    end
end
function Terminal:autoCompleteInput(input)
    return {}
end
function Terminal:getPrompt(prompt, opts)
    local user = self.opts.user
    local hostname = window.location.hostname
    local hostnameshort = hostname:match("^(.-)%.") or hostname
    return self.opts.prompt
        :gsub("\\u", user)
        :gsub("\\H", hostname)
        :gsub("\\h", hostnameshort)
end

function Terminal:createElements(opts)
    local ctr = document:createElement('div')
    ctr.classList:add('terminal')
    local fragment = document:createDocumentFragment()
    local el = document:createElement('div')
    el.className = 'terminal-output'
    el:setAttribute('spellcheck', false)
    local intro = document:createElement('div')
    intro.innerHTML = opts.intro
    el:appendChild(intro)
    local line = document:createElement('p')
    local prompt = document:createElement('span')
    prompt.className = 'terminal-prompt'
    prompt.innerHTML = self:getPrompt(opts.prompt, opts)
    line:appendChild(prompt)
    local input = document:createElement('span')
    input.className = 'terminal-input'
    input:setAttribute('contenteditable', true)
    line:appendChild(input)
    el:appendChild(line)
    fragment:appendChild(el)
    ctr:appendChild(fragment)
    self.output = el
    return ctr
end
function Terminal:init(ctId, opts)
    self.opts = opts or {}
    local el = (type(ctId)=="string" and document.getElementById(ctId)) or ctId
    local els = self:createElements(opts)
    el:appendChild(els)

    self.output:addEventListener('keydown', function(_, ev)
        if (ev.keyCode == self.KEY_TAB) then
            local p = ev.target
            local s = self:autoCompleteInput(p.textContent:gsub("%s+", ''))
            if (#s == 1) then
                local range = document:createRange()
                local sel = window:getSelection()
                range:setStart(p.childNodes[0], #s[0])
                range:collapse(true)
                sel:removeAllRanges()
                sel:addRange(range)
            end
            ev:preventDefault()
            return false
        end
    end)
    self.output:addEventListener('keyup', function(_, ev)
        if (self.historyIndex > 0) then
            print(self.historyIndex)
            self:browseHistory(ev.target, ev.keyCode)
            return
        end
    end)
    self.output:addEventListener('keypress', function(_, ev)
        local prompt = ev.target
        if (ev.keyCode ~= 13) then return end
        self:updateHistory(prompt.textContent)
        local input = prompt.textContent:match( "^%s*(.-)%s*$" )
        local input_split = {}
        for arg in input:gmatch("[a-zA-Z0-9_%-]+") do table.insert(input_split, arg) end
        if (input_split[1]) then
            self:tryCommand(self.output, input_split[1], input)
        end
        self:resetPrompt(self.output, prompt)
        ev:preventDefault()
    end)
    self.output:addEventListener("click", function(_, ev)
        local tmp = self.output:querySelectorAll('.terminal-input[contenteditable=true]')
        local el = tmp[#tmp - 1]
        local sel = window:getSelection()
        if (sel.isCollapsed and sel.rangeCount <= 1 and el) then
            el:focus()
        end
    end, false)
    self.output:querySelector(".terminal-input"):focus()
end