local js = require"js"
local window = js.global
local document = window.document


function initTerm()
    print("term loading")
    Terminal:init(document.body, {
        commands = {},
        prompt = "\\u@\\H $ ",
        intro = "<p>Welcome to mitchellmonahan.com. Type \'help\' to get started.</p><p>&nbsp;</p>",
        user = "user"
    })
    print('ready')
end