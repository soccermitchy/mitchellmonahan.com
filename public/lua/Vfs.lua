Vfs = {
    -- Return values
    E_OK = 0,
    E_NOTABSOLUTE = 1,
    E_NOTFOUND = 2,
    E_NOTADIRECTORY = 3,
    E_NOTAFILE = 4,
    E_EXISTS = 5,

    -- Data storage
    inodes = {},
    root_node = -1,
    next_node = 1
}

-- example inode structure:
local _ = {
    this = 0, -- ID of current inode
    parent = 0, -- ID of parent inode - same as 'this' for the root node,
    children = { -- ID of children inodes
        name1 = 0,
        name2 = 1,
        name3 = 2,
        -- ...etc for parent nodes
    },
    type = "directory", -- or file
    content = "" -- empty/nil for directory, string for file.
}
_ = nil


-- Some utilty functions
function Vfs:isAbsolute(path)
    return path:sub(1,1)=="/"
end
function Vfs:getAbsoluteFromRelative(pwd, path) 
    if not self:isAbsolute(name) then
        return self.E_NOTABSOLUTE
    end

    local pwdSplit = split(pwd, "/")
    local relSplit = split(path, "/")
    local newPath = pwdSplit
    for k,v in pairs(relSplit) do
        if (v ~= ".") then -- current directory, we want to just skip that
            if (v=="..") then -- parent directory
                table.remove(newPath, #newPath) -- remove last entry from new path (to reflect going up a dir)
            end
            table.insert(newPath, v) -- add directory/file to new path
        end
    end
    return self.E_OK, "/"..table.concat(newPath, "/")
end
function Vfs:debug(...)
    if true then print(...) end
end
-- Basically, allows stuff like repeating slashes (ie. /usr/////bin) which is a valid feature apperantly. We just drop empty paths in the list.
function Vfs:stringToPathList(str)
    local pathList = split(str, "/")
    local newPathList = {}
    for k,v in pairs(pathList) do
        if v~= "" then
            table.insert(newPathList, v)
        end
    end
    return newPathList
end
function Vfs:pathListToString(pl)
    if type(pl)=="string" then return pl end
    return "/"..table.concat(pl, "/")
end

-- Normal filesystem functions
function Vfs:init()
    self.inodes = {}
    -- We can't use :createNode for the root node - it has no parent to add to.
    self.inodes[self.next_node]={
        this = self.next_node,
        parent = self.next_node,
        children = {},
        type = "directory"
    }
    -- Set the root node ID
    self.root_node = self.next_node
    -- Increment next node counter
    self.next_node = self.next_node + 1
end
function Vfs:createNode(name, parent, children, type, content)
    if (self.inodes[self.next_node] ~= nil) then
        error("Sanity check failed - next node is not available! (next_node = " .. self.next_node..")")
    end
    if (self.inodes[parent].children[name]) then
        return self.E_EXISTS
    end
    -- Insert the actual node
    self.inodes[self.next_node]={
        this = self.next_node,
        parent = parent,
        type = type,
        content = content,
        children = children
    }
    -- Add new node to parent
    self.inodes[parent].children[name]=self.next_node
    -- Increment next node counter
    self.next_node = self.next_node + 1
    return self.E_OK
end
function Vfs:deleteNode(id)
    -- We don't really 'delete' a node - we just unlink it. Maybe I'll implement
    -- old inode purging later along with collapsing the tree, but that's not really a priority right now.
    if (not self.inodes[id]) then
        return self.E_NOTFOUND
    end

    -- Well first, we have to find the name of the inode in it's parent...
    -- May be easier later to make each inode have a two-way mapping.
    local parentChildren = self.inodes[self.inodes[id].parent].children
    local name = ""
    -- Go through all the parents' children and find the name of the one with the ID we're removing
    for k,v in pairs(parentChildren) do
        if v==id then
            name = k
        end
    end
    if name=="" then
        return self.E_OK -- uh, ok.. the inode already was removed from it's parent?
    end
    -- well, we got the name, lets unlink the node from the parent.
    self.inodes[self.inodes[id].parent].children[name]=nil
    return self.E_OK
end
function Vfs:getEnt(path)
    if (type(path) == "string" and not self:isAbsolute(path)) then
        return self.E_NOTABSOLUTE
    end
    -- small short-circuit here - if the path is /, just return the root node
    if (path == "/" or #path==0) then
        return self.inodes[self.root_node]
    end
    local splitPath
    -- Get the path as the list and set up some stuff so we can pretty easily track where we are.
    if (type(path)=="string") then
        splitPath = self:stringToPathList(path)
    else
        splitPath = path
        path=self:pathListToString(splitPath)
    end
    --local currentLevel = 1
    local currentNodeId = self.root_node
    
    for k=1,#splitPath do
        local currentNode = self.inodes[currentNodeId]
        if (currentNode.type == "directory" and currentNode.children[splitPath[k]]) then
            currentNodeId = currentNode.children[splitPath[k]]
        else
            return self.E_NOTFOUND
        end
    end 
    --self:debug("Reached undefined functionality in getEnt("..path..")")
    return self.E_OK, self.inodes[currentNodeId]        
end
function Vfs:createFile(path, content)
    if (not self:isAbsolute(path)) then
        return self.E_NOTABSOLUTE
    end
    local child = self:stringToPathList(path) -- Get the split path for the new file
    local parent = self:stringToPathList(path) -- Make a copy to store the path of the parent
    parent[#parent]=nil -- We're getting the parent dir just by removing the last element from the list.
    local error, inode = self:getEnt("/"..table.concat(parent, "/")) -- Get the parent inode so we can link to it
    if (error ~= self.E_OK) then -- We couldn't get the inode, oh well
        return error
    end
    --print("MKFILE using inode " .. inode.this .. " as parent")
    -- Create a new node for the child
    --print("Next inode id - " .. self.next_node)
    local error = self:createNode(child[#child], inode.this, {}, "file", content)
    return error -- we're gonna just return what createNode returns anyway, might as well not check it
end
function Vfs:createDirectory(path)
    if (not self:isAbsolute(path)) then
        return self.E_NOTABSOLUTE
    end
    local newPath = self:stringToPathList(path)
    local parentPath = self:stringToPathList(path)
    parentPath[#parentPath] = nil
    local inode
    if (#parentPath == 0) then
        inode = self.inodes[self.root_node]
    else
        local error
        error, inode = self:getEnt(parentPath)
        if (error ~= self.E_OK) then
            return error
        end
    end
    return self:createNode(newPath[#newPath], inode.this, {}, "directory")
end
function Vfs:remove(path)
    if (not self:isAbsolute(path)) then
        return self.E_NOTABSOLUTE
    end
    -- We don't need to split the path here - we don't need to extract data
    -- for it outside of getEnt.
    local error, inode = self:getEnt(path)
    if (error ~= self.E_OK) then
        return error
    end
    return self:deleteNode(inode.this)
end
